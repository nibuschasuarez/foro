import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { InicioComponent } from './inicio/inicio.component';
import { CarneComponent } from './carne/carne.component';
import { EnsaladasComponent } from './ensaladas/ensaladas.component';
import { HuevoComponent } from './huevo/huevo.component';
import { PastaComponent } from './pasta/pasta.component';

const routes: Routes = [
  	{ path: '', component: InicioComponent },
    { path: 'carne', component: CarneComponent },
    { path: 'ensaladas', component: EnsaladasComponent },
    { path: 'huevo', component: HuevoComponent },
    { path: 'pasta', component: PastaComponent },


];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ForoRoutingModule { }
