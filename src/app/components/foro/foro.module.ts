import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ForoRoutingModule } from './foro-routing.module';


import { CarneComponent } from './carne/carne.component';
import { HuevoComponent } from './huevo/huevo.component';
import { PastaComponent } from './pasta/pasta.component';
import { EnsaladasComponent } from './ensaladas/ensaladas.component';
import { InicioComponent } from './inicio/inicio.component';


@NgModule({
  declarations: [
    CarneComponent,
    HuevoComponent,
    PastaComponent,
    EnsaladasComponent,
    InicioComponent
  ],
  imports: [
    CommonModule,
    ForoRoutingModule
  ]
})
export class ForoModule { }
