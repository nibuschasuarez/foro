import { Injectable } from '@angular/core';
// import { elementAt } from 'rxjs';
import { IDatos } from '../interfaces/cita';

@Injectable({
  providedIn: 'root'
})
export class CitaService {
  LISTA_CITAS: IDatos[] = [];
  constructor() { }

  getCitas(){
    if(localStorage.getItem('citasLis')===null){
      return this.LISTA_CITAS;
    }else{
      this.LISTA_CITAS=JSON.parse(localStorage.getItem('citasLis')||"[]")
      return this.LISTA_CITAS
  }
  }
  agregarCita(cita:IDatos){
    this.LISTA_CITAS.push(cita);
    let citaLis: IDatos[] = [];
    if(localStorage.getItem('citasLis')===null){
      citaLis.push(cita)
      localStorage.setItem('citasLis',JSON.stringify(citaLis))
    }
    else{
      citaLis=JSON.parse(localStorage.getItem('citasLis') || "[]")
      citaLis.push(cita);
      localStorage.setItem('citasLis',JSON.stringify(citaLis));
  }
  }
  eliminarCita(nombre:string){
    this.LISTA_CITAS=this.LISTA_CITAS.filter(data => data.nombre !== nombre)
    console.log(this.LISTA_CITAS);
    localStorage.setItem('citasLis',JSON.stringify(this.LISTA_CITAS));
    this.getCitas();
    
   
  }


   modificarCita(names:IDatos){
    this.eliminarCita(names.nombre);
    this.agregarCita(names);
  }

  buscarCita(id:string):IDatos{
    return this.LISTA_CITAS.find(element => element.nombre === id) || {} as IDatos;
  }
}
